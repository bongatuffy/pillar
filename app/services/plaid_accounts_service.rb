class PlaidAccountsService
  def initialize(current_user)
    @current_user = current_user
  end

  def retrieve_accounts
    @current_user.plaid_items.map { |item| retrieve_accounts_for_item(item) }.flatten
  end

  private

  def retrieve_accounts_for_item(item)
    PLAID_CLIENT.auth.get(item.access_token).accounts
  end
end
