class PlaidItemsService

  def initialize(current_user:, public_token:, institution:)
    @current_user = current_user
    @public_token = public_token
    unpack_institution_param(institution)
  end

  def handle_item
    exchange_public_token
    item_accounts = retrieve_item_accounts

    if is_duplicate_item?(item_accounts)
      return nil
    else
      @new_item = create_item
      new_accounts = create_accounts(item_accounts)
    end
  end

  private

  def exchange_public_token
    exchange_token_response = PLAID_CLIENT.item.public_token.exchange(@public_token)
    @access_token = exchange_token_response.access_token
    @item_id = exchange_token_response.item_id
  end

  def unpack_institution_param(institution)
    @institution_name = institution['name']
    @institution_id = institution['institution_id']
  end

  def retrieve_item_accounts
    PLAID_CLIENT.auth.get(@access_token).accounts
  end

  def is_duplicate_item?(accounts)
    any_accounts_duplicate?(accounts)
  end

  def any_accounts_duplicate?(accounts)
    accounts.any? { |account| is_duplicate_account?(account) }
  end

  def is_duplicate_account?(account)
    @current_user.plaid_accounts.where(
      name: account.name,
      official_name: account.official_name,
      mask: account.mask,
      institution_name: @institution_name,
      institution_id: @institution_id
    ).present?
  end

  def create_item
    PlaidItem.create(
      user_id: @current_user.id,
      access_token: @access_token,
      item_id: @item_id,
      institution_name: @institution_name,
      institution_id: @institution_id
    )
  end

  def create_accounts(accounts)
    accounts.map { |account| create_account(account) }
  end

  def create_account(account)
    PlaidAccount.create(
      user_id: @current_user.id,
      plaid_item_id: @new_item.id,
      name: account.name,
      official_name: account.official_name,
      mask: account.mask,
      institution_name: @institution_name,
      institution_id: @institution_id
    )
  end
end
