class StudentLoansController < ApplicationController
  def create
    @loan = current_user.student_loans.new(student_loan_params)

    if @loan.save
      render json: @loan, status: 200
    else
      render json: @loan.errors.full_messages, status: 401
    end
  end

  def index
    render json: current_user.student_loans.to_json, status: 200
  end

  private

  def student_loan_params
    params.require(:loan).permit(:loan_type, :loan_status, :original_loan_amount,
                                  :outstanding_balance, :interest_rate)
  end
end
