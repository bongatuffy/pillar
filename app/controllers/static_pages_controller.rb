class StaticPagesController < ApplicationController
  def root
    @plaid_public_key = ENV['PLAID_PUBLIC_KEY']
    @plaid_env = ENV['PLAID_ENV']
  end
end
