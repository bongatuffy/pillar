class PlaidItemsController < ApplicationController
  def create
    new_accounts = PlaidItemsService.new(
      current_user: current_user,
      public_token: params['public_token'],
      institution: params['institution']
    ).handle_item
    if new_accounts
      render json: new_accounts.to_json, status: 200
    else
      render json: { errors: ['Item already exists.']}, status: 401
    end
  end

  def index
    accounts = PlaidAccountsService.new(current_user).retrieve_accounts
    if accounts
      render json: accounts.to_json, status: 200
    else
      render json: { errors: ['No accounts'] }, status: 401
    end
  end
end
