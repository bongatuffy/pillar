class UsersController < ApplicationController
  def create
    @user = User.new(user_params)

    if @user.save
      login(@user)
      render json: @user
    else
      render json: @user.errors.full_messages
    end
  end

  private

  def user_params
    params.require(:user).permit( :email, :password, :first_name, :last_name, :address,
                                  :city, :state, :zip_code, :date_of_birth, :ssn_last_four)
  end
end
