class PlaidAccount < ApplicationRecord
  belongs_to :plaid_item
  belongs_to :user
end
