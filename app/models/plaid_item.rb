class PlaidItem < ApplicationRecord
  belongs_to :user
  has_many :plaid_accounts
end
