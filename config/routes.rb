Rails.application.routes.draw do
  root 'static_pages#root'
  get '/home' => 'static_pages#root'
  resources :users, only: [:create], format: :json
  resource :session, only: [:create, :destroy, :show], format: :json
  resources :plaid_items, only: [:create, :index]
  resources :student_loans, only: [:create, :index]
end
