class AddInstitutionToPlaidItems < ActiveRecord::Migration[5.2]
  def change
    add_column(:plaid_items, :institution_id, :string)
    add_column(:plaid_items, :institution_name, :string)
  end
end
