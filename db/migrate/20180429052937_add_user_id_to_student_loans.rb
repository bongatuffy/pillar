class AddUserIdToStudentLoans < ActiveRecord::Migration[5.2]
  def change
    add_column(:student_loans, :user_id, :integer)
  end
end
