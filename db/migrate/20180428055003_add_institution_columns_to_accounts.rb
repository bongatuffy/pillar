class AddInstitutionColumnsToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column(:plaid_accounts, :institution_name, :string)
    add_column(:plaid_accounts, :institution_id, :string)
  end
end
