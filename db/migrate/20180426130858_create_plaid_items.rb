class CreatePlaidItems < ActiveRecord::Migration[5.2]
  def change
    create_table :plaid_items do |t|
      t.integer :user_id
      t.string :access_token
      t.string :item_id
      t.timestamps
    end
  end
end
