class AddUserIdToPlaidAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column(:plaid_accounts, :user_id, :integer)
  end
end
