class CreatePlaidAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :plaid_accounts do |t|
      t.integer :plaid_item_id
      t.string :name
      t.string :official_name
      t.string :mask
      t.timestamps
    end
  end
end
