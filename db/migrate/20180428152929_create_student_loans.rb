class CreateStudentLoans < ActiveRecord::Migration[5.2]
  def change
    create_table :student_loans do |t|
      t.string :loan_type
      t.string :loan_status
      t.float :original_loan_amount
      t.float :outstanding_balance
      t.float :interest_rate
    end
  end
end
