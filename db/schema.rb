# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_04_29_052937) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "plaid_accounts", force: :cascade do |t|
    t.integer "plaid_item_id"
    t.string "name"
    t.string "official_name"
    t.string "mask"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.string "institution_name"
    t.string "institution_id"
  end

  create_table "plaid_items", force: :cascade do |t|
    t.integer "user_id"
    t.string "access_token"
    t.string "item_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "institution_id"
    t.string "institution_name"
  end

  create_table "student_loans", force: :cascade do |t|
    t.string "loan_type"
    t.string "loan_status"
    t.float "original_loan_amount"
    t.float "outstanding_balance"
    t.float "interest_rate"
    t.integer "user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", null: false
    t.string "password_digest", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "session_token", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "address"
    t.string "city"
    t.string "state"
    t.integer "zip_code"
    t.date "date_of_birth"
    t.integer "ssn_last_four"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["session_token"], name: "index_users_on_session_token", unique: true
  end

end
