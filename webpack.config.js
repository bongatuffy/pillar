var webpack = require('webpack');
var path = require('path');

module.exports = (env) => {
  const devtool = env && env.production ? false : 'eval-source-map';

  return {
    entry: './frontend/index.js',
    output: {
      path: path.resolve(__dirname, 'app', 'assets', 'javascripts'),
      filename: 'bundle.js',
    },
    module: {
      rules: [
        {
          test: [/\.jsx?$/],
          exclude: /(node_modules)/,
          loader: 'babel-loader',
          query: {
            presets: ['es2015', 'react']
          }
        },
        {
          test: /\.css$/,
          use: [ 'style-loader', 'css-loader' ]
        },
        {
          test: /\.scss$/,
          use: [{
              loader: "style-loader" // creates style nodes from JS strings
          }, {
              loader: "css-loader" // translates CSS into CommonJS
          }, {
              loader: "sass-loader" // compiles Sass to CSS
          }]
        },
        {
          test: /\.(png|svg|jpg|gif)$/,
          use: [
            'file-loader'
          ]
        }
      ]
    },
    devtool: devtool,
    resolve: {
      extensions: ['.js', '.jsx', '*'],
      alias: {
        components: path.resolve(__dirname, 'frontend/components'),
        features: path.resolve(__dirname, 'frontend/features'),
        reducers: path.resolve(__dirname, 'frontend/reducers'),
        styles: path.resolve(__dirname, 'frontend/styles'),
        selectors: path.resolve(__dirname, 'frontend/selectors')
      }
    },
    plugins: [
      new webpack.DefinePlugin({
        "process.env": {
          NODE_ENV: JSON.stringify("production")
        }
      })
    ]
  };
};
