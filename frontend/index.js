import React from 'react';
import ReactDOM from 'react-dom'
import configureStore from './store';
import Root from './components/root';
import Immutable from 'immutable';

function checkCurrentUser() {
  let store;
  const loggedInState = {
    session: Immutable.fromJS({ currentUser: window.currentUser })
  };

  if (window.currentUser) {
    store = configureStore(loggedInState);
  } else {
    store = configureStore();
  }

  return store;
}


document.addEventListener('DOMContentLoaded', () => {
  const store = checkCurrentUser();
  const root = document.getElementById('root');
  ReactDOM.render(<Root store={store} />, root);
});
