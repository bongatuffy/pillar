import { connect } from 'react-redux';

import SplashPage from '../components';
import { openModal, closeModal } from 'reducers/session-reducer';

const mapStateToProps = (state, ownProps) => {
  return {
    isModalOpen: state.session.get('isModalOpen'),
    currentUser: state.session.get('currentUser')
  };
};

const mapDispatchToProps = dispatch => {
   return {
     openModal: () => dispatch(openModal()),
     closeModal: () => dispatch(closeModal())
   }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SplashPage);
