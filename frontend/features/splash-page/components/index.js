import React from 'react';
import PropTypes from 'prop-types';

import Pane from 'components/pane';
import Button from 'components/button';
import Section from 'components/section';
import Modal from 'components/modal';
import Session from 'features/session/containers';
import Header from './header';
import MeetPillarSection from './meet-pillar-section';

import '../styles/index.scss';

const namespace = "splash-page"

export default class SplashPage extends React.Component {
  render() {
    return (
      <div className={namespace}>
        <Header
          openModal={this.props.openModal}
        />
        <MeetPillarSection />

        <Modal
          className={`${namespace}__session-modal`}
          isOpen={this.props.isModalOpen}
          closeModal={this.props.closeModal}
        >
          <Session />
        </Modal>
      </div>
    )
  }
};

SplashPage.propTypes = {
  isModalOpen: PropTypes.bool,
  openModal: PropTypes.func,
  closeModal: PropTypes.func,
  currentUser: PropTypes.string
};
// <GettingStartedSection />
// <StayTunedSection />
// <Footer />
