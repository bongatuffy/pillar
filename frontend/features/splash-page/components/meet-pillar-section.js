import React from 'react';

import Pane from 'components/pane';
import Button from 'components/button';
import Section from 'components/section';
import Article from 'components/article';
import Figure from 'components/figure';

import '../styles/index.scss';

const namespace = 'splash-page__meet-pillar-section'

export default class MeetPillarSection extends React.Component {
  render() {
    const figureProps = {
      figureOne: {
        image: window.images.lightbulb,
        title: 'Specific recommendations',
        details: 'Personalized advice and plans that are truly best for you.'
      },
      figureTwo: {
        image: window.images.rocket,
        title: 'Any topic',
        details: 'Budgeting, saving, credit, banking, loans, investing, etc.'
      },
      figureThree: {
        image: window.images.computer,
        title: 'Easy to use, easy to understand',
        details: 'Financial planning with Pillar couldn’t be simpler. We do it for you'
      },
      figureFour: {
        image: window.images.flower,
        title: 'Available instantly 24/7',
        details: 'Get the help you need anytime, anywhere'
      }
    }

    return (
      <Section className={namespace}>
        <Pane className={`${namespace}__pane`}>
          <Article
            className={`${namespace}__pane__article`}
            headline='Meet Pillar. Your roadmap to financial well-being.'
          >
            <div className={`${namespace}__pane__article__content`}>
              <Figure {...figureProps.figureOne}/>
              <Figure {...figureProps.figureTwo}/>
              <Figure {...figureProps.figureThree}/>
              <Figure {...figureProps.figureFour}/>
            </div>
          </Article>
        </Pane>
      </Section>
    )
  }
};
