import React from 'react';
import PropTypes from 'prop-types';

import Pane from 'components/pane';
import Button from 'components/button';
import Section from 'components/section';

import '../styles/index.scss';

const namespace = 'splash-page__header';

export default class Header extends React.Component {
  constructor(props) {
    super(props)
    this.openModal = this.openModal.bind(this);
  }

  openModal() {
    this.props.openModal();
  }

  render() {
    return (
      <Section className={namespace}>
        <Pane className={`${namespace}__pane`}>
          <div className={`${namespace}__pane__top-row`}>
            <img src={window.images.logo} />
            <Button
              isClear={true}
              whiteText={true}
              text="SIGN IN"
              onClick={this.openModal}
            />
          </div>
          <div className={`${namespace}__pane__headline`}>
            <h1>Your personal financial advisor</h1>
            <h2>Get simple, fast, and certain answers to all your financial questions</h2>
          </div>

          <Button
            className={`${namespace}__pane__button`}
            text="GET STARTED"
            isArrow={true}
          />
        </Pane>
      </Section>
    );
  }
};

Header.propTypes = {
  openModal: PropTypes.func
}
