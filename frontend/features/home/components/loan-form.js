import React from 'react';
import PropTypes from 'prop-types';

import TextInput from 'components/forms/text-input';
import Button from 'components/button';

export default class LoanForm extends React.Component {
  constructor(props) {
    super(props)
    this.onChangeTextInput = this.onChangeTextInput.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChangeTextInput(e) {
    const { name, value } = e.currentTarget
    const payload = { fieldName: name, value }
    this.props.onChangeTextInput(payload);
  }

  onSubmit() {
    const params = this.props.formData.toJS();
    this.props.onSubmit(params);
  }

  render() {
    return (
      <div>
        <h1>{"Add Student Loans"}</h1>
        <TextInput
          onChange={this.onChangeTextInput}
          fieldName='loanType'
          placeholder='Loan Type'
          value={this.props.formData.get('loanType')}
        />
        <TextInput
          onChange={this.onChangeTextInput}
          fieldName='loanStatus'
          placeholder='Loan Status'
          value={this.props.formData.get('loanStatus')}
        />
        <TextInput
          onChange={this.onChangeTextInput}
          fieldName='originalLoanAmount'
          placeholder='Original Loan Amount'
          value={this.props.formData.get('originalLoanAmount')}
        />
        <TextInput
          onChange={this.onChangeTextInput}
          fieldName='outstandingBalance'
          placeholder='Outstanding Balance'
          value={this.props.formData.get('outstandingBalance')}
        />
        <TextInput
          onChange={this.onChangeTextInput}
          fieldName='interestRate'
          placeholder='Interest Rate'
          value={this.props.formData.get('interestRate')}
        />

        <Button
          text='Submit'
          isBlue={true}
          whiteText={true}
          onClick={this.onSubmit}
        />
      </div>
    )
  }
}

LoanForm.propTypes = {
  formData: PropTypes.object,
  onChangeTextInput: PropTypes.func,
  onSubmit: PropTypes.func
}
