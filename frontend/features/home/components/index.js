import React from 'react';
import PropTypes from 'prop-types';

import Header from './header';
import Summary from './summary';
import Button from 'components/button';

import '../styles/index.scss';

const namespace = 'home';

export default class Home extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.retrieveBankAccounts();
    this.props.retrieveLoans();
  }

  componentDidUpdate() {
     if (this.props.shouldRetrieveBankAccounts) this.props.retrieveBankAccounts();
  }

  render () {
    return (
      <div>
        <div className={`${namespace}__header-background`} />
        <Header
          logout={this.props.logout}
        />
        <Summary
          currentUser={this.props.currentUser}
          createPlaidItem={this.props.createPlaidItem}
          openModal={this.props.openModal}
          isModalOpen={this.props.isModalOpen}
          closeModal={this.props.closeModal}
          formData={this.props.formData}
          onChangeTextInput={this.props.onChangeTextInput}
          submitLoan={this.props.submitLoan}
          netWorth={this.props.netWorth}
          studentLoanBalance={this.props.studentLoanBalance}
        />
      </div>
    );
  }
};

Home.propTypes = {
  logout: PropTypes.func,
  currentUser: PropTypes.string,
  retrieveBankAccounts: PropTypes.func,
  netWorth: PropTypes.number,
  shouldRetrieveBankAccounts: PropTypes.bool,
  isModalOpen: PropTypes.bool,
  closeModal: PropTypes.func,
  studentLoanFormData: PropTypes.object,
  onChangeTextInput: PropTypes.func,
  submitLoan: PropTypes.func,
  studentLoanBalance: PropTypes.number,
  retrieveLoans: PropTypes.func
}
