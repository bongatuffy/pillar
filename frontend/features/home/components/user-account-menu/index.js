import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

const namespace = 'user-account-menu';

export default class UserAccountMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isMenuOpen: false };
    this.onClick = this.onClick.bind(this);
    this.renderMenuCaret = this.renderMenuCaret.bind(this);
  }

  renderMenu() {
    if (this.state.isMenuOpen) {
      return (
        <div className={`${namespace}--menu`}>
          <div
            className={`${namespace}--menu--menu-item`}
            onClick={this.props.logout}
          >Log out</div>
        </div>
      )
    }
  }

  renderMenuCaret() {
    if (this.state.isMenuOpen) return 'fas fa-angle-up';
    return 'fas fa-angle-down';
  }

  onClick() {
    this.setState({ isMenuOpen: !this.state.isMenuOpen });
  }

  render() {
    return (
      <div className={namespace}>
        <div
          className={`${namespace}--account-text`}
          onClick={this.onClick}
        >
          ACCOUNT
          <span><i className={this.renderMenuCaret()}></i></span>
          {this.renderMenu()}
        </div>
      </div>
    )
  }
};

UserAccountMenu.propTypes = {
  logout: PropTypes.func
}
