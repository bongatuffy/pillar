import React from 'react';
import PropTypes from 'prop-types';

import Section from 'components/section';
import Pane from 'components/pane';
import PlaidIntegration from '../plaid-integration';
import Modal from 'components/modal';
import LoanForm from '../loan-form';
import Button from 'components/button';
import DataCard from 'components/data-card';

import './styles.scss';

const namespace = 'summary';

export default class Summary extends React.Component {
  render() {
    return (
      <Section>
        <Pane className={`${namespace}__pane`}>
          <div className={`${namespace}__pane--content`}>
            <h1>{`Welcome, ${this.props.currentUser.get('email')}`}</h1>
            <p>
              {"Welcome to Pillar! Get started by connecting your bank accounts or adding your student loan information."}
            </p>

            <div className={`${namespace}__pane--buttons`}>
              <PlaidIntegration
                createPlaidItem={this.props.createPlaidItem}
              />
              <Button
                isBlue={true}
                text='Add Student Loans'
                whiteText={true}
                onClick={this.props.openModal}
                className={`${namespace}__pane--buttons--student-loans`}
              />
            </div>
          </div>

          <div className={`${namespace}__pane--data-cards`}>
            <DataCard
              title={'NET WORTH'}
              data={this.props.netWorth}
            />
            <DataCard
              className={`${namespace}__pane--data-cards--data-card`}
              title={'TOTAL LOAN BALANCE'}
              data={this.props.studentLoanBalance}
            />
          </div>

          <Modal
            isOpen={this.props.isModalOpen}
            closeModal={this.props.closeModal}
          >
            <LoanForm
              formData={this.props.formData}
              onChangeTextInput={this.props.onChangeTextInput}
              onSubmit={this.props.submitLoan}
            />
          </Modal>
        </Pane>
      </Section>
    )
  }
};

Summary.propTypes = {
  currentUser: PropTypes.object
}
