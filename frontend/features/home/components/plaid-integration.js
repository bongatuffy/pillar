import React from 'react';
import Button from 'components/button';
import PropTypes from 'prop-types';

export default class PlaidIntegration extends React.Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    const createPlaidItem = this.props.createPlaidItem;
    var handler = Plaid.create({
      apiVersion: 'v2',
      clientName: 'Pillar',
      env: window.plaidEnv,
      product: ['transactions'],
      key: window.plaidPublicKey,
      onSuccess: (public_token, { institution }) => {
        createPlaidItem(public_token, institution)
      }
    });

    handler.open();
  }

  render() {
    return (
      <Button
        isBlue={true}
        whiteText={true}
        text={'Add bank account'}
        onClick={this.onClick}
      />
    );
  };
};

PlaidIntegration.propTypes = {
  createPlaidItem: PropTypes.func
}
