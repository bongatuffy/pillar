import React from 'react';
import PropTypes from 'prop-types';

import Section from 'components/section';
import Pane from 'components/pane';
import Button from 'components/button';
import UserAccountMenu from './user-account-menu';

import '../styles/index.scss';

const namespace = 'home-page-header';

export default class Header extends React.Component {
  render () {
    return (
      <Section className={`${namespace}__section`}>
        <Pane className={`${namespace}__pane`}>
          <img src={window.images.logo} />
          <div className={`${namespace}__pane--buttons`}>
            <Button
              isYellow={true}
              isChat={true}
              text="ASK ADVISOR"
              className={`${namespace}__pane--buttons--ask-advisor-button`}
            />
            <UserAccountMenu
              logout={this.props.logout}
            />
          </div>
        </Pane>
      </Section>
    )
  }
};

Header.propTypes = {
  logout: PropTypes.func,
  currentUser: PropTypes.object
};
