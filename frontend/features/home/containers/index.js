import { connect } from 'react-redux';

import Home from '../components';
import { logout } from 'reducers/session-reducer';
import { createPlaidItem, retrieveBankAccounts } from 'reducers/plaid-reducer';
import { onChangeTextInput, submitLoan, retrieveLoans, openModal, closeModal } from 'reducers/student-loan-reducer';
import * as plaidSelectors from 'selectors/plaid-selectors';
import * as loanSelectors from 'selectors/loan-selectors';

const mapStateToProps = (state) => {
  return {
    currentUser: state.session.get('currentUser'),
    netWorth: plaidSelectors.calculateNetWorth(state),
    shouldRetrieveBankAccounts: state.plaid.get('shouldRetrieveBankAccounts'),
    isModalOpen: state.studentLoan.get('isModalOpen'),
    formData: state.studentLoan.get('formData'),
    studentLoanBalance: loanSelectors.calculateStudentLoanBalance(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(logout()),
    createPlaidItem: (public_token, institution) => dispatch(createPlaidItem(public_token, institution)),
    retrieveBankAccounts: () => dispatch(retrieveBankAccounts()),
    openModal: () => dispatch(openModal()),
    closeModal: () => dispatch(closeModal()),
    onChangeTextInput: (payload) => dispatch(onChangeTextInput(payload)),
    submitLoan: (loan) => dispatch(submitLoan(loan)),
    retrieveLoans: () => dispatch(retrieveLoans())
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
