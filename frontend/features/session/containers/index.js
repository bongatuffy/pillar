import { connect } from 'react-redux';

import Session from '../components';
import { switchForm, signup, login, logout, onChangeTextInput } from 'reducers/session-reducer';

const mapStateToProps = (state) => {
  return {
    currentUser: state.session.get('currentUser'),
    isSignupForm: state.session.get('isSignupForm'),
    signupForm: state.session.get('signupForm'),
    loginForm: state.session.get('loginForm')
  };
};

const mapDispatchToProps = dispatch => {
  return {
    switchForm: () => dispatch(switchForm()),
    signup: (user) => dispatch(signup(user)),
    login: (user) => {
      return dispatch(login(user))
    },
    onChangeTextInput: (payload) => {
      return dispatch(onChangeTextInput(payload))
    }
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Session);
