import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route } from 'react-router';
import Home from 'features/home/containers'

import LoginForm from './login-form';
import SignupForm from './signup-form';

import '../styles/index.scss';

const namespace = 'session';

export default class Session extends React.Component {
  constructor(props) {
    super(props);
    this.renderForm = this.renderForm.bind(this);
  }

  renderForm() {
    if (!this.props.isSignupForm) {
      return (
        <LoginForm
          switchForm={this.props.switchForm}
          onChangeTextInput={this.props.onChangeTextInput}
          formData={this.props.loginForm}
          onSubmit={this.props.login}
          formName='loginForm'
        />
      )
    }

    return (
      <SignupForm
        switchForm={this.props.switchForm}
        onChangeTextInput={this.props.onChangeTextInput}
        formData={this.props.signupForm}
        onSubmit={this.props.signup}
        formName='signupForm'
      />
    )
  }

  render() {
    return (
      <div>
        {this.renderForm()}
      </div>
    )
  }
}

Session.propTypes = {
  isSignupForm: PropTypes.bool,
  switchForm: PropTypes.func,
  signup: PropTypes.func,
  login: PropTypes.func,
  onChangeTextInput: PropTypes.func,
  form: PropTypes.object,
  currentUser: PropTypes.string
}
