import React from 'react';
import PropTypes from 'prop-types';

import TextInput from 'components/forms/text-input';
import Button from 'components/button';

const namespace = 'login-form';

export default class LoginForm extends React.Component {
  constructor(props) {
    super(props)
    this.renderSignupLink = this.renderSignupLink.bind(this);
    this.renderSubmitButton = this.renderSubmitButton.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeTextInput = this.onChangeTextInput.bind(this);
  }

  onSubmit() {
    const params = {
      email: this.props.formData.get('email'),
      password: this.props.formData.get('password')
    }
    this.props.onSubmit(params);
  }

  renderSubmitButton() {
    return (
      <Button
        text='Submit'
        isBlue={true}
        onClick={this.onSubmit}
      />
    )
  }

  renderSignupLink() {
    return (
      <a
        href='#'
        onClick={this.props.switchForm}
      >
        New user? Click here to sign up.
      </a>
    )
  }

  onChangeTextInput(e) {
    const { name, value } = e.currentTarget;
    const formName = this.props.formName;
    this.props.onChangeTextInput({ formName, fieldName: name, value })
  }

  render() {
    return (
      <div>
        <h1>{this.props.header}</h1>
        <TextInput
          onChange={this.onChangeTextInput}
          fieldName='email'
          placeholder='Email address'
          value={this.props.formData.get('email')}
        />
        <TextInput
          onChange={this.onChangeTextInput}
          fieldName='password'
          isPassword={true}
          placeholder='Password'
          value={this.props.formData.get('password')}
        />

        {this.renderSubmitButton()}
        {this.renderSignupLink()}
      </div>
    )
  }
};

LoginForm.propTypes = {
  isSignupForm: PropTypes.bool,
  switchForm: PropTypes.func,
  onSubmit: PropTypes.func,
  onChangeTextInput: PropTypes.func,
  formData: PropTypes.object
};
