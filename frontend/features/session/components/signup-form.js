import React from 'react';
import PropTypes from 'prop-types';

import TextInput from 'components/forms/text-input';
import Button from 'components/button';

const namespace = 'signup-form';

export default class SignupForm extends React.Component {
  constructor(props) {
    super(props)
    this.renderSignupLink = this.renderSignupLink.bind(this);
    this.renderSubmitButton = this.renderSubmitButton.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeTextInput = this.onChangeTextInput.bind(this);
  }

  onSubmit() {
    const params = this.props.formData.toJS();
    this.props.onSubmit(params);
  }

  renderSubmitButton() {
    return (
      <Button
        text='Submit'
        isBlue={true}
        onClick={this.onSubmit}
      />
    )
  }

  renderSignupLink() {
    return (
      <a
        href='#'
        onClick={this.props.switchForm}
      >
        Already signed up? Click here to log in.
      </a>
    )
  }

  onChangeTextInput(e) {
    const { name, value } = e.currentTarget;
    const formName = this.props.formName;
    const payload = { formName, fieldName: name, value };
    this.props.onChangeTextInput(payload)
  }

  render() {
    return (
      <div>
        <h1>{"Create an Account. It's free."}</h1>
        <TextInput
          onChange={this.onChangeTextInput}
          fieldName='email'
          placeholder='Email address'
          value={this.props.formData.get('email')}
        />
        <TextInput
          onChange={this.onChangeTextInput}
          fieldName='password'
          isPassword={true}
          placeholder='Password'
          value={this.props.formData.get('password')}
        />
        <TextInput
          onChange={this.onChangeTextInput}
          fieldName='firstName'
          placeholder='First Name'
          value={this.props.formData.get('firstName')}
        />
        <TextInput
          onChange={this.onChangeTextInput}
          fieldName='lastName'
          placeholder='Last Name'
          value={this.props.formData.get('lastName')}
        />
        <TextInput
          onChange={this.onChangeTextInput}
          fieldName='address'
          placeholder='Address'
          value={this.props.formData.get('address')}
        />
        <TextInput
          onChange={this.onChangeTextInput}
          fieldName='city'
          placeholder='City'
          value={this.props.formData.get('city')}
        />
        <TextInput
          onChange={this.onChangeTextInput}
          fieldName='state'
          placeholder='State'
          value={this.props.formData.get('state')}
        />
        <TextInput
          onChange={this.onChangeTextInput}
          fieldName='zipCode'
          placeholder='Zip Code'
          value={this.props.formData.get('zipCode')}
        />
        <TextInput
          onChange={this.onChangeTextInput}
          fieldName='dateOfBirth'
          placeholder='DOB (MM/DD/YYYY)'
          value={this.props.formData.get('dateOfBirth')}
        />
        <TextInput
          onChange={this.onChangeTextInput}
          fieldName='ssnLastFour'
          isPassword={true}
          placeholder='SSN (Last 4 Digits)'
          value={this.props.formData.get('ssnLastFour')}
        />

        {this.renderSubmitButton()}
        {this.renderSignupLink()}
      </div>
    )
  }
};

SignupForm.propTypes = {
  isSignupForm: PropTypes.bool,
  switchForm: PropTypes.func,
  onSubmit: PropTypes.func,
  onChangeTextInput: PropTypes.func,
  formData: PropTypes.object
};
