import React from 'react';
import './styles.scss';
import classNames from 'classnames';

const namespace = 'pane';

export default class Page extends React.Component {
  render () {
    const paneClass = classNames({
      [namespace]: true,
      [this.props.className]: this.props.className
    })

    return (
      <div className={paneClass}>
        {this.props.children}
      </div>
    );
  }
}
