import React from 'react';
import PropTypes from 'prop-types';
import CurrencyFormat from 'react-currency-format';

import './styles.scss';

const namespace = 'data-card';

export default class DataCard extends React.Component {
  render() {
    return (
      <div className={`${namespace}  ${this.props.className}`}>
        <div className={`${namespace}--content`}>
          <h2>{this.props.title}</h2>
          <figure>
            <CurrencyFormat
              thousandSeparator=','
              decimalScale={2}
              fixedDecimalScale={true}
              prefix='$'
              displayType='text'
              value={Number(this.props.data)}
            />
          </figure>
        </div>
      </div>
    )
  }
};
