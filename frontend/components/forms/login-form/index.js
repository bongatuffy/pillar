import React from 'react';
import PropTypes from 'prop-types';

import TextInput from 'components/forms/text-input';
import Button from 'components/button';

import './styles.scss';

const namepsace = 'login-form';

export default class LoginForm extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div>
        <h1>Sign In</h1>
        <TextInput />
        <TextInput />
        <Button
          text='Sign in'
        />
        <a href='#'>New user? Click here to sign up</a>
      </div>
    )
  }
};

LoginForm.propTypes = {

};
