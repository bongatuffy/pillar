import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

const namespace = 'text-input';

export default class TextInput extends React.Component {
  constructor(props) {
    super(props);
    this.renderInputType = this.renderInputType.bind(this);
  }

  renderInputType() {
    if (this.props.isPassword) return 'password';
    return 'text';
  }

  render() {
    return (
      <div className={`${namespace}__container`}>
        <label>{this.props.label}</label>
        <input
          name={this.props.fieldName}
          onChange={this.props.onChange}
          placeholder={this.props.placeholder}
          type={this.renderInputType()}
          value={this.props.value}
        />
      </div>
    )
  }
}

TextInput.propTypes = {
  label: PropTypes.string,
  fieldName: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  isPassword: PropTypes.bool
}
