import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

const namespace = 'figure';

export default class Figure extends React.Component {
  render() {
    return (
      <div className={namespace}>
        <div className={`${namespace}--image`}>
          <img src={this.props.image} />
        </div>
        <div className={`${namespace}--text`}>
          <h3>{this.props.title}</h3>
          <p>{this.props.details}</p>
        </div>
      </div>
    );
  }
}
