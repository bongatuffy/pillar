import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

const namespace = 'article';

export default class Article extends React.Component {
  render() {
    return (
      <div className={namespace}>
        <h2>{this.props.headline}</h2>
        {this.props.children}
      </div>
    );
  }
}

Article.propTypes = {
  headline: PropTypes.string
}
