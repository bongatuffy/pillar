import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './styles.scss';

const namespace = 'button';

export default class Button extends React.Component {
  renderArrow() {
    if (this.props.isArrow) return <span><i className='fas fa-arrow-right'></i></span>;
    return null;
  }

  renderChatIcon() {
    if (this.props.isChat) return <span><i className='fas fa-comments'></i></span>;
    return null;
  }

  render() {
    const btnClass = classNames({
      [namespace]: true,
      [this.props.className]: true,
      [`${namespace}--clear`]: this.props.isClear,
      [`${namespace}--white-text`]: this.props.whiteText,
      [`${namespace}--blue`]: this.props.isBlue,
      [`${namespace}--yellow`]: this.props.isYellow
    });

    return (
      <button
        className={btnClass}
        onClick={this.props.onClick}
      >
        <div className={`${namespace}__content`}>
          {this.renderChatIcon()}
          <div>{this.props.text}</div>
          {this.renderArrow()}
        </div>
      </button>
    )
  }
}

Button.propTypes = {
  className: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
  isClear: PropTypes.bool,
  isBlue: PropTypes.bool,
  whiteText: PropTypes.bool,
  isYellow: PropTypes.bool
}
