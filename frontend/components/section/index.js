import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import './styles.scss';

const namespace = 'section';

export default class Section extends React.Component {
  render () {
    const sectionClass = classNames({
      [namespace]: true,
      [this.props.className]: this.props.className
    });


    return (
      <div className={sectionClass}>
        {this.props.children}
      </div>
    )
  }
}

Section.propTypes = {
  className: PropTypes.string
}
