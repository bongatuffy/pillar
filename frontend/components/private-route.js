import React from 'react';
import { Route, Redirect } from 'react-router';

const PrivateRoute = ({ component: Component, redirectTo, redirectCondition, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        redirectCondition ? (
          <Redirect
            to={{
              pathname: redirectTo,
              state: { from: props.location }
            }}
          />
        ) : (
          <Component {...props} />
        )
      }
    />
  );
};

export default PrivateRoute;
