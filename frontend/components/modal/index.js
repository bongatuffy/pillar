import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';

import './styles.scss';

const namespace = 'modal';

export default class SessionModal extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const modalStyle = {
      content : {
        backgroundColor: 'rgba(0, 0, 0, 0)',
        border: 'none',
        position: 'absolute',
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        transform: 'translate(-50%, -50%)',
        maxWidth: '680px',
        borderRadius: '8px',
        padding: '20px'
      },
      overlay: {
        backgroundColor: 'rgba(0, 0, 0, 0.3)'
      }
    }

    return (
      <Modal
        style={modalStyle}
        isOpen={this.props.isOpen}
      >
        <button
          className={`${namespace}--close-button`}
          onClick={this.props.closeModal}
        >
          x
        </button>
        <div className={`${namespace}--content`}>
          {this.props.children}
        </div>
      </Modal>
    );
  }
};

SessionModal.propTypes = {
  isOpen: PropTypes.bool,
  closeModal: PropTypes.func
};
