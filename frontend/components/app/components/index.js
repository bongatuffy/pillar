import React from 'react';
import _ from 'underscore';

import style from 'styles/index.scss';
import SplashPage from 'features/splash-page/containers';
import { Route, Redirect } from 'react-router'
import PrivateRoute from 'components/private-route'
import Home from 'features/home/containers';

export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <PrivateRoute
          exact
          path='/'
          redirectTo='/home'
          redirectCondition={this.props.currentUser}
          component={SplashPage}
        />
        <PrivateRoute
          exact
          path='/home'
          redirectTo='/'
          redirectCondition={!this.props.currentUser}
          component={Home}
        />
      </div>
    )
  }
};

// const App = ({ children }) => (
//   <div className="app">
//     <Switch>
//       <Route exact path='/' component={SplashPage}
//     </Switch>
//   </div>
// );
//
// export default App;
