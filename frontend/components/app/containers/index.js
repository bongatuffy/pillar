import { connect } from 'react-redux';
import App from '../components';
import { withRouter } from 'react-router';

const mapStateToProps = (state) => {
  return {
    currentUser: state.session.get('currentUser')
  };
};

const mapDispatchToProps = dispatch => {
  return {
  }
};

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(App));
