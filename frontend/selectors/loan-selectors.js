export const calculateStudentLoanBalance = (state) => {
  const loans = state.studentLoan.get('loans');
  return loans.reduce((result, loan) => {
    const balance = Number(loan.get('outstandingBalance'));
    return result + balance;
  }, 0);
}
