import camelize from 'camelize';
import Immutable from 'immutable';

export const prepareBankAccounts = (accountsArray) => {
  const accounts = Immutable.fromJS(camelize(accountsArray));
  return accounts.reduce((result, account) => {
    return result.set(account.get('accountId'), account);
  }, Immutable.OrderedMap());
}

export const calculateNetWorth = (state) => {
  const accounts = state.plaid.get('bankAccounts');
  return accounts.reduce((result, account) => {
    const currentBalance = Number(account.getIn(['balances', 'current']));

    switch(account.get('type')) {
      case 'depository':
        return result + currentBalance;
      case 'credit':
        return result - currentBalance;
      default:
        return result
    }
  }, 0);
}
