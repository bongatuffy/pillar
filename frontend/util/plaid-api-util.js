export const createPlaidItem = (public_token, institution) => {
  return $.ajax({
    method: "POST",
    url: '/plaid_items',
    data: { public_token: public_token, institution }
  });
}

export const retrieveBankAccounts = () => {
  return $.ajax({
    method: "GET",
    url: '/plaid_items'
  });
}
