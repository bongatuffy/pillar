export const submitLoan = (loan) => {
  return $.ajax({
    method: 'POST',
    url: '/student_loans',
    data: {loan}
  });
};

export const retrieveLoans = () => {
  return $.ajax({
    method: "GET",
    url: '/student_loans'
  })
}
