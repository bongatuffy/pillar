import Immutable from 'immutable';
import * as APIUtil from '../util/plaid-api-util';
import * as selectors from 'selectors/plaid-selectors';

const CREATE_PLAID_ITEM_SUCCESS = 'CREATE_PLAID_ITEM_SUCCESS'
const GET_ACCOUNTS_SUCCESS = "GET_ACCOUNTS_SUCCESS";
const GET_ACCOUNTS_FAILURE = "GET_ACCOUNTS_FAILURE";

export function createPlaidItem(public_token, institution) {
  return (dispatch) => {
    return APIUtil.createPlaidItem(public_token, institution)
    .then(accounts => dispatch(createPlaidItemSuccess(accounts)));
  }
}

export function retrieveBankAccounts() {
  return (dispatch) => {
    return APIUtil.retrieveBankAccounts()
      .then(accounts => dispatch(getAccountsSuccess(accounts)),
            err => dispatch(getAccountsFailure(err.responseJSON)));
  };
};

export const createPlaidItemSuccess = () => {
  return {
    type: CREATE_PLAID_ITEM_SUCCESS
  }
}

export const getAccountsSuccess = accounts => {
  return {
    type: GET_ACCOUNTS_SUCCESS,
    payload: accounts
  };
}

export const getAccountsFailure = errors => {
  return {
    type: GET_ACCOUNTS_FAILURE,
    payload: errors
  };
}


const defaultState = Immutable.fromJS({
  shouldRetrieveBankAccounts: false,
  bankAccounts: []
});

const plaidReducer = (state = defaultState, action) => {
  switch(action.type) {
    case CREATE_PLAID_ITEM_SUCCESS:
      return state.set('shouldRetrieveBankAccounts', true);
    case GET_ACCOUNTS_SUCCESS:
      const bankAccounts = Immutable.fromJS(action.payload);
      return state.merge({
        bankAccounts: bankAccounts,
        shouldRetrieveBankAccounts: false
      });
    default:
      return state;
  };
};

export default plaidReducer;
