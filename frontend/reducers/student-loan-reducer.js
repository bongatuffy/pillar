import Immutable from 'immutable';
import snakeize from 'snakeize';
import camelize from 'camelize';

import * as APIUtil from '../util/student-loan-api-util'

const ON_CHANGE_TEXT_INPUT = 'ON_CHANGE_TEXT_INPUT';
const SUBMIT_LOAN_SUCCESS = 'SUBMIT_LOAN_SUCCESS';
const RETRIEVE_LOANS_SUCCESS = 'RETRIEVE_LOANS_SUCCESS';
const OPEN_LOAN_MODAL = "OPEN_LOAN_MODAL";
const CLOSE_MODAL = "CLOSE_MODAL";

export function submitLoan(loan) {
  const snakeizedLoan = snakeize(loan);
  return (dispatch) => {
    return APIUtil.submitLoan(snakeizedLoan)
      .then(loan => dispatch(submitLoanSuccess(loan)));
  };
};

export function retrieveLoans() {
  return (dispatch) => {
    return APIUtil.retrieveLoans()
      .then(loans => dispatch(retrieveLoansSuccess(loans)));
  }
}

export const onChangeTextInput = ({ fieldName, value }) => {
  return {
    type: ON_CHANGE_TEXT_INPUT,
    payload: { fieldName, value }
  };
};

export const submitLoanSuccess = (loan) => {
  return {
    type: SUBMIT_LOAN_SUCCESS,
    payload: loan
  };
};

export const retrieveLoansSuccess = (loans) => {
  return {
    type: RETRIEVE_LOANS_SUCCESS,
    payload: loans
  }
}


export const openModal = () => {
  return {
    type: OPEN_LOAN_MODAL
  }
}

export const closeModal = () => {
  return {
    type: CLOSE_MODAL
  }
}

const defaultState = Immutable.fromJS({
  loans: [],
  formData: {
    loanType: '',
    loanStatus: '',
    originalLoanAmount: '',
    outstandingBalance: '',
    interestRate: ''
  },
  isModalOpen: false
});

const studentLoanReducer = (state = defaultState, action) => {
  switch(action.type) {
    case ON_CHANGE_TEXT_INPUT:
      return state.setIn(['formData', action.payload.fieldName], action.payload.value);
    case SUBMIT_LOAN_SUCCESS:
      const loan = Immutable.fromJS(camelize(action.payload));
      const newLoans = state.get('loans').push(loan);
      return state.merge({
        loans: newLoans,
        isModalOpen: false
      });
    case RETRIEVE_LOANS_SUCCESS:
      const loans = Immutable.fromJS(camelize(action.payload));
      return state.set('loans', loans);
    case OPEN_LOAN_MODAL:
      return state.set('isModalOpen', true);
    case CLOSE_MODAL:
      return state.set('isModalOpen', false);
    default:
      return state;

  };
};

export default studentLoanReducer;
