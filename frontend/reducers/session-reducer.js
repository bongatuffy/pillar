import Immutable from 'immutable';
import camelize from 'camelize';

import * as APIUtil from '../util/session-api-util';

const OPEN_MODAL = "OPEN_MODAL";
const CLOSE_MODAL = "CLOSE_MODAL";
const SWITCH_FORM = "SWITCH_FORM";
const RECEIVE_CURRENT_USER = "RECEIVE_CURRENT_USER";
const RECEIVE_ERRORS = "RECEIVE_ERRORS";
const CLEAR_ERRORS = "CLEAR_ERRORS";
const ON_CHANGE_TEXT_INPUT = "ON_CHANGE_TEXT_INPUT";

const AUTH_SUCCESS = "AUTH_SUCCESS";
const AUTH_FAILURE = "AUTH_FAILURE";
const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";

export function signup(user) {
  return (dispatch) => {
    return APIUtil.signup({ user })
      .then(user => dispatch(authSuccess(user)),
            err => dispatch(authFailure(err.responseJSON)));
  };
}

export function login(user) {
  return (dispatch) => {
    return APIUtil.login(user)
      .then(user => dispatch(authSuccess(user)),
            err => dispatch(authFailure(err.responseJSON)));
  };
}

export function logout() {
  return (dispatch) => {
    return APIUtil.logout()
      .then(user => dispatch(logoutSuccess()),
            err => dispatch(authFailure(err.responseJSON)));
  };
}

export const authSuccess = user => ({
  type: AUTH_SUCCESS,
  payload: user
});

export const logoutSuccess = () => ({
  type: LOGOUT_SUCCESS
});

export const authFailure = errors => ({
  type: AUTH_FAILURE,
  errors
})

export const openModal = () => {
  return {
    type: OPEN_MODAL
  }
};

export const closeModal = () => {
  return {
    type: CLOSE_MODAL
  }
};

export const switchForm = () => {
  return {
    type: SWITCH_FORM
  }
}

export const onChangeTextInput = ({ formName, fieldName, value }) => {
  return {
    type: ON_CHANGE_TEXT_INPUT,
    payload: { formName, fieldName, value }
  }
}

const defaultState = Immutable.fromJS({
  currentUser: null,
  errors: [],
  isModalOpen: false,
  isSignupForm: false,
  loginForm: {
    email: '',
    password: ''
  },
  signupForm: {
    email: '',
    password: '',
    firstName: '',
    lastName: '',
    address: '',
    city: '',
    state: '',
    zipCode: '',
    dateOfBirth: '',
    ssnLastFour: ''
  }
});

const sessionReducer = (state = defaultState, action) => {
  const initialState = defaultState.merge(state);

  switch(action.type) {
    case OPEN_MODAL:
      return initialState.set('isModalOpen', true);
    case CLOSE_MODAL:
      return initialState.set('isModalOpen', false);
    case SWITCH_FORM:
      const isSignupForm = initialState.get('isSignupForm');
      return initialState.set('isSignupForm', !isSignupForm);
    case ON_CHANGE_TEXT_INPUT:
      return initialState.setIn([action.payload.formName, action.payload.fieldName], action.payload.value)
    case AUTH_SUCCESS:
      const user = Immutable.fromJS(camelize(action.payload));
      return initialState.merge({
        'currentUser': user,
        'loginForm': defaultState.get('loginForm'),
        'signupForm': defaultState.get('signupForm'),
        'isModalOpen': false
      });
    case AUTH_FAILURE:
      return initialState.set('errors', action.payload);
    case LOGOUT_SUCCESS:
      return initialState.set('currentUser', null);
    default:
      return initialState;
  }
};

export default sessionReducer;
