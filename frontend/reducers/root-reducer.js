import {combineReducers} from 'redux';
import SessionReducer from './session-reducer';
import PlaidReducer from './plaid-reducer';
import StudentLoanReducer from './student-loan-reducer';

const RootReducer = combineReducers({
  session: SessionReducer,
  plaid: PlaidReducer,
  studentLoan: StudentLoanReducer
});

export default RootReducer;
